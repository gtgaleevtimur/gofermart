# Накопительная система лояльности «Гофермарт»
![gophermart_2x_1_1636364562.png](img%2Fgophermart_2x_1_1636364562.png)

### Общие требования
Система представляет собой HTTP API со следующими требованиями к бизнес-логике:
- регистрация, аутентификация и авторизация пользователей;
- приём номеров заказов от зарегистрированных пользователей;
- учёт и ведение списка переданных номеров заказов зарегистрированного пользователя;
- учёт и ведение накопительного счёта зарегистрированного пользователя;
- проверка принятых номеров заказов через систему расчёта баллов лояльности;
- начисление за каждый подходящий номер заказа положенного вознаграждения на счёт лояльности пользователя.

### Абстрактная схема взаимодействия с системой
Ниже представлена абстрактная бизнес-логика взаимодействия пользователя с системой:
- пользователь регистрируется в системе лояльности «Гофермарт».
- пользователь совершает покупку в интернет-магазине «Гофермарт».
- заказ попадает в систему расчёта баллов лояльности.
- пользователь передаёт номер совершённого заказа в систему лояльности.
- система связывает номер заказа с пользователем и сверяет номер с системой расчёта баллов лояльности.
- при наличии положительного расчёта баллов лояльности производится начисление баллов лояльности на счёт пользователя.
- пользователь списывает доступные баллы лояльности для частичной или полной оплаты последующих заказов в интернет-магазине «Гофермарт».

Примечания:

a) пункт 2 представлен как гипотетический и не реализован в данной работе;

б) пункт 3 реализован в системе расчёта баллов лояльности и не реализован в данной работе.
 
# Сводное HTTP API
Накопительная система лояльности «Гофермарт» предоставляет следующие HTTP-хендлеры:
- POST /api/user/register — регистрация пользователя;
- POST /api/user/login — аутентификация пользователя;
- POST /api/user/orders — загрузка пользователем номера заказа для расчёта;
- GET /api/user/orders — получение списка загруженных пользователем номеров заказов, статусов их обработки и информации о начислениях;
- GET /api/user/balance — получение текущего баланса счёта баллов лояльности пользователя;
- POST /api/user/balance/withdraw — запрос на списание баллов с накопительного счёта в счёт оплаты нового заказа;
- GET /api/user/balance/withdrawals — получение информации о выводе средств с накопительного счёта пользователем.

# Конфигурирование сервиса накопительной системы лояльности
Сервис поддерживает конфигурирование следующими методами:
- адрес и порт запуска сервиса: переменная окружения RUN_ADDRESS или флаг -a;
- адрес подключения к базе данных: переменная окружения DATABASE_URI или флаг -d;
- адрес системы расчёта начислений: переменная окружения ACCRUAL_SYSTEM_ADDRESS или флаг -r.

# Система расчетов баллов лояльности
Система расчета баллов лояльности является внешним сервисом в доверенном контуре. Он работает по принципу чёрного ящика и недоступен для инспекции внешними клиентами. Система рассчитывает положенные баллы лояльности за совершённый заказ по сложным алгоритмам, которые могут меняться в любой момент времени.

Внешнему потребителю доступна только информация о количестве положенных за конкретный заказ баллов лояльности. Причины наличия или отсутствия начислений внешнему потребителю неизвестны.

